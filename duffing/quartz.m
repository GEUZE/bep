function F = quartz(x) % r = x(1), phi = x(2)
global w0 delta gamma alpha w
F = [(1/(2*w)) * (-w*delta*x(1) - gamma*sin(x(2)));
     (1/(2*w)) * (-(w^2 - w0^2) + (3/4)*alpha*x(1)^2 - gamma*cos(x(2))/x(1))];
 

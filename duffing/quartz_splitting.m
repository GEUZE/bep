close all
clear all
clc

global w0 epsilon delta gamma alpha w
w0 = 1;
epsilon = 1;
delta = 0.2;
gamma = 2.5;
alpha = 0.03;



gamma_span = [0.3 0.6 0.9 1.2 1.5]; %1.1 1.3 1.4
N = 2000;

G=5;
wspan = linspace(0.001,2*w0,N);
options = optimset('display','off','TolFun',0.5e-10);

w0 = 1.016

for k = 1:G;
    clear f_up up f_unstable unstable f_down down
    x0 = [0.01 0.01];
    gamma = gamma_span(k);
    for i = 1:N;
        w = wspan(i);
        [A,fval,exitflag] = fsolve(@quartz,x0,options); % Call solver
        if exitflag ~= 1;
            break
        end
        f_up(i) = w;
        up(i,:) = A;
        x0 = up(i,:);
        jump_down = i;
    end
    
    % jump_down = 2000;
    
    if jump_down ~= 99999
        
        
        x0(1) = 0.975 * x0(1);
        x0(2) = (1/0.975) * x0(2);
        
        for i = 1:jump_down-1;
            w = wspan(jump_down - i);
            [A,fval,exitflag] = fsolve(@quartz,x0,options); % Call solver
            if exitflag ~= 1;
                break
            end
            f_unstable(i) = w;
            unstable(i,:) = A;
            x0(1) = 0.975*unstable(i,1);
            x0(2) = (1/0.975)*unstable(i,2);
            jump_up = i;
        end
        
        x0(1) = 0.94 * x0(1);
        x0(2) = (1/0.98) * x0(2);
        
        for i = 1:(N - jump_down + jump_up)
            w = wspan(jump_down - jump_up + i);
            [A,fval,exitflag] = fsolve(@quartz,x0,options); % Call solver
            if exitflag ~= 1;
                break
            end
            f_down(i) = w;
            down(i,:) = A;
            x0(1) = 0.975 * down(i,1);
            x0(2) = (1/0.975) * down(i,2);
        end
        
        frequency_up{k} = f_up;
        frequency_unstable{k} = f_unstable;
        frequency_down{k} = f_down;
        
        amplitude_up{k} = up(:,1);
        amplitude_unstable{k} = unstable(:,1);
        amplitude_down{k} = down(:,1);
        
        phase_up{k} = up(:,2);
        phase_unstable{k} = unstable(:,2);
        phase_down{k} = down(:,2);
        
        frequency{k} = horzcat(f_up,f_down(jump_up:end));
        amplitude{k} = vertcat(abs(up(:,1)),abs(down(jump_up:end,1)));
        phase{k} = pi - vertcat(abs(up(:,2)),abs(down(jump_up:end,2)));
        
    else
        frequency_up{k} = f_up;
        amplitude_up{k} = up(:,1);
        phase_up{k} = up(:,2);
        
        frequency{k} = f_up;
        amplitude{k} = up(:,1);
        phase{k} = up(:,2);
        
    end
    legenda{k} = horzcat('$\gamma$ = ',num2str(gamma));
    figure(1)
    hold on
    plot(frequency{k},amplitude{k})
    hold off
    figure(2)
    hold on
    plot(frequency{k},phase{k}),
    hold off
end
figure(1)
xlabel('$\omega/\omega_0$')
ylabel('$r$')
legend(legenda)
pos1 = get(gca,'Position');
figure(2)
xlabel('$\omega/\omega_0$')
ylabel('$\phi$')
axis([0 2 0 pi])
ax = gca;
set(ax,'YTick',[0,pi/2,pi]);
set(ax,'YTickLabel',{'$-\pi$','$-\pi/2$',0});
%legend(legenda)

load('1.mat')
figure(3)
hold on
plot(frequency{1},amplitude{1}+amplitude1{1})
plot(frequency{1},amplitude{2}+amplitude1{2})
plot(frequency{1},amplitude{3}+amplitude1{3})
plot(frequency{1},amplitude{4}+amplitude1{4})
plot(frequency{1},amplitude{5}+amplitude1{5})
figure(4)
hold on
plot(frequency{1},(amplitude{1}+amplitude1{1})/(max(amplitude{1}+amplitude1{1})))
plot(frequency{1},(amplitude{2}+amplitude1{2})/(max(amplitude{2}+amplitude1{2})))
plot(frequency{1},(amplitude{3}+amplitude1{3})/(max(amplitude{3}+amplitude1{3})))
plot(frequency{1},(amplitude{4}+amplitude1{4})/(max(amplitude{4}+amplitude1{4})))
plot(frequency{1},(amplitude{5}+amplitude1{5})/(max(amplitude{5}+amplitude1{5})))
xlabel('$\omega/\omega_0$')
ylabel('$r/r_\mathrm{max}$')
legend(legenda)
xlim([0.75 1.6])

\clearpage\section{Results \& Discussion}
We will start with a basic characterization of the quartz crystal and proceed to experiment with its amplitude-frequency dependence. Subsequently, the effect of putting multiple crystals in parallel is researched. Finally, the crystal is coated, and the response of the system to different gasses is investigated. 

\subsection{Orientation}
A broad frequency response of the quartz crystal is shown in \autoref{fig:orientation_scan}. The upward curvature of the graph is intrinsic to the circuit as predicted from the peak in \autoref{fig:sim_crystal} and is too steep to be attributed to the decrease of the parasitic impedance of $C_0$ with frequency. The frequency scan shows that apart from the large resonance peak around the crystal manufacturer's specified parallel resonance frequency of \SI{4.608}{MHz}, there are many smaller peaks corresponding to different resonance modes. The third harmonic shear mode is clearly visible at approximately three times the fundamental frequency. 

\begin{figure}[!htb]
	\centering
		\includegraphics[width=\textwidth]{figures/orientation_scan}
	\caption{Frequency response of a crystal with a fundamental parallel resonance frequency of \SI{4.608}{\mega\hertz} at a driving voltage of $V_\mathrm{ref}=\SI{40}{\milli\volt}$.}
	\label{fig:orientation_scan}
\end{figure}

Whe zooming in on the fundamental frequency, as shown in \autoref{fig:fundamental_scan_low}, the first thing to notice is that the actual resonance frequency is slightly lower than the crystal manufacturer's specified parallel resonance frequency of \SI{4.608}{MHz}. This is as expected, since we know from \autoref{sec:model} that the series resonant frequency is always lower than the parallel resonance region, and the circuit used is a \emph{series} resonant circuit while the specified frequency is for a \emph{parallel} resonance circuit. 

At a low driving amplitude of $V_\mathrm{ref}=\SI{20}{\milli\volt}$, the amplitude-frequency dependence is negligible, and the forward and backward frequency sweeps are aligned. Using the peak position and peak width, the Q-factor is approximately \SI{150000}{} and the impedance at resonance is approximately \SI{20}{\ohm}. 

\begin{figure}
	\centering
		\includegraphics{figures/linear}
	\caption{Normalized frequency response of the crystal close to its fundamental parallel resonance frequency of \SI{4.608}{\mega\hertz} at a driving voltage of $V_\mathrm{ref}=\SI{20}{\milli\volt}$.}
	\label{fig:fundamental_scan_low}
\end{figure} 

The fundamental frequency, which has the largest response, is not necessarily the most useful response for this experiment. First of all, a large response corresponds to a high current. Apart from the difficulty of making a high frequency source which can source high currents, this leads to several undesirable effects, such as heating up of the crystal and voltage drop in parasitic inductances due to the crystal leads and wires. When several crystals are connected in parallel, their currents are summed, which further increases the effect of the parasitic inductance. This voltage drop introduces a deviation from an ideal voltage source and acts as cross-talk between the crystals. 

\begin{figure}[!htb]
	\centering
		\includegraphics{figures/fundamental_scan}
	\caption{Frequency responses of the crystal around a its fundamental parallel resonance frequency of approximately \SI{4.608}{\mega\hertz} at various driving voltages.}	
	\label{fig:fundamental_scan}
\end{figure} 

\begin{figure}[!b]
	\centering
		\includegraphics{figures/fundamental_scan_high}
	\caption{Normalized frequency response of the crystal around its fundamental parallel resonance frequency of \SI{4.608}{\mega\hertz} at a driving voltage of $V_\mathrm{ref}=\SI{200}{\milli\volt}$.}
	\label{fig:fundamental_scan_high}
\end{figure} 

To illustrate the problem of self-heating, \autoref{fig:fundamental_scan} shows the frequency response measured at different driving voltages. Until $V_\mathrm{ref}>\SI{140}{\milli\volt}$, the resonance frequency resembles that of a Duffing equation as predicted in \autoref{fig:gamma}. We see significant amplitude-frequency dependence and corresponding to a stiffening spring constant. However, as higher driving voltage the resonance peak shifts back to the left, and at $V_\mathrm{ref}=\SI{200}{\milli\volt}$, the amplitude of the resonance peak is decreased in comparison with the peak of a response of a lower driving amplitude. \autoref{fig:fundamental_scan_high} gives more insight into what is happening. At high driving voltage, the crystal heats up during the high-amplitude part of the sweep, which modifies its frequency response. The initial slope of the backward sweep is higher because of nonlinearity, so here the resonance frequency is reached with a shorter heat-up time. The amount of nonlinearity seems to be decreased at higher temperature. Referring to the model parameters in the Duffing equation (\autoref{eq:duffing}) this is most likely due to an increase in damping $\delta$, because a decrease in the nonlinear spring constant $\alpha$ would correspond to an increase in amplitude at resonance (see \autoref{fig:alpha}), while we see a decrease in amplitude. Although the exact angle at which the crystal is cut is not specified, by comparing with the frequency-temperature graphs of \autoref{fig:AT}, it seems that the crystal is cut at an angle of more than 35$^\circ$10', since the frequency response shifts to the left at higher temperature. This effect is detrimental for a sensor using the resonant frequency as indication of present gases, but can be used to construct a relaxation oscillator as described in detail in \autoref{ap:relaxation}. 

The second reason why the large fundamental response is not necessarily the ideal response is that the level of amplitude-frequency dependence does not seem to be directly related to the size of the response. For example, the crystal doesn't become bistable at the fundamental resonance mode at the used amplitudes, but the resonance peak at approximately $\SI{4.8354}{MHz}$, which has an amplitude of about 5 times as low as the fundamental resonance peak, is highly nonlinear as can be seen from \autoref{fig:4-84_scan}. A high level of amplitude-frequency dependence is desirable, since lower amplitudes are needed to reach bistability, which eases the requirements of the driving system. As can be seen in \autoref{fig:4-84_scan_high}, This resonance mode shows less temperature drift, and has a clear bistable area with two jump points. 

\begin{figure}[!t]
	\centering
		\includegraphics{figures/4-84_scan}
	\caption{Frequency responses of the crystal around a spurious resonance mode at approximately \SI{4.8354}{\mega\hertz} at various driving voltages.}
	\label{fig:4-84_scan}
\end{figure} 


Some modes of the crystal have such a high resistance than they barely show any nonlinearity when driven at the amplitude where bistability occurs in \autoref{fig:4-84_scan}. These spurious modes have unstable resonance frequencies as shown in \autoref{fig:high_R}. The different types of modes show a great variability in the amount of nonlinearity, damping, and frequency stability. 

\begin{figure}[!t]
	\centering
		\includegraphics{figures/4-84_scan_high}
	\caption{Normalized frequency response of the crystal around a spurious resonance mode at approximately \SI{4.8354}{\mega\hertz} showing hysteresis at a driving voltage of $V_\mathrm{ref}=\SI{200}{\milli\volt}$.}
	\label{fig:4-84_scan_high}
\end{figure}

The mode around $\SI{4.8}{MHz}$ will be used in further experiments, due to the significant amplitude-frequency dependence, while having an amplitude of about 5 times lower than the fundamental resonance mode. The bistability area of this resonance mode can be found in \autoref{fig:hysteresis_area}. The resonance frequency of the spurious mode is a few kHz higher than in the previous figures, because this is a different specimen of the same type of crystal, while the crystal has not been calibrated for this particular spurious mode. 

\begin{figure}[!htb]
	\centering
		\includegraphics{figures/high_R}
	\caption{Frequency responses of the crystal around a spurious resonance mode at approximately \SI{5.24}{\mega\hertz} at various driving voltages.\label{fig:high_R}}
\end{figure}

\begin{figure}[!htb]
	\centering
		\includegraphics{figures/PEE_before_hysteresis}
	\caption{Hysteresis area of the spurious resonance mode around $\SI{4.8}{MHz}$. The figure is obtained by superimposing the upward and downward frequency sweep traces.\label{fig:hysteresis_area}}
\end{figure}

\subsection{Parallel crystals}
\begin{figure}[!b]
	\centering
  \begin{subfigure}{13.7cm}
		\includegraphics{figures/splitting_up}
  \end{subfigure}
	\\
	\begin{subfigure}{13.7cm}
		\includegraphics{figures/splitting_down}
  \end{subfigure}
	\caption{Normalized upward frequency sweeps (top) and downward frequency sweeps (bottom) at different driving voltages for two crystals with very closely separated resonance frequencies connected in parallel. \label{fig:splitting}}
\end{figure}


In this section the behavior of quartz crystals connected in parallel will be demonstrated. The increased distinguishability of resonance peaks of parallel crystals using the nonlinear jump phenomenon is shown in \autoref{fig:splitting}. Two crystals connected in parallel are driven at different amplitudes. At low driving amplitudes, the response looks like the response of an single crystal and the linear resonance peaks are indiscernible. At higher driving amplitudes, the nonlinear jump phenomenon reveals the individual resonance frequencies. For the linear peaks to be discernible, they have to be separated by at least the bandwidth, which is approximately \SI{30}{\hertz} for these crystals. When they are driven strongly nonlinear however, the vertical jump points can be as closely spaced as the measurement device step size allows and still be discernible, which is about \SI{1}{\hertz} in this example. 

The frequency responses at constant driving amplitude of three crystals with closely spaced resonance frequencies connected in parallel, alongside the three individual responses of the crystals are shown in \autoref{fig:parallel}. This figure illustrates that the resonant frequencies of the crystals are barely affected by each other, indicating that their cross-coupling is negligible. It is interesting to note that the amplitude of the sum of the three parallel crystals is actually below the individual response of crystal 3 just after the jump of the second crystal. This can be explained by the theory of \autoref{sec:crystal_resonance}, where \autoref{fig:Z_crystal_example} in particular shows that the phase is reversed after the point of resonance, which decreases the total amplitude when the currents of all crystals are summed. 

\begin{figure}[!htb]
	\centering
		\includegraphics[width=\textwidth]{figures/parallel.pdf}
	\caption{Frequency responses of three parallel uncoated crystals with closely spaced resonance frequencies driven at $V_\mathrm{ref} = \SI{200}{\milli\volt}$. The frequency responses of the individual crystals are dashed.}
	\label{fig:parallel}
\end{figure}

The jump points of the parallel response are close to the jump points of the individual crystals, but not identical. There are two factors which likely give rise to this discrepancy. Firstly there will always be some crosstalk between the crystals, since the driving circuit is not a perfect voltage source. As the jump points of the frequency response will shift when a gas is sensed, the crosstalk will manifest itself differently for different mixtures of gasses, so it possibly degrades the measurement in an unpredictable way. Secondly, since this experiment is conducted in open air, fluctuations in temperature and humidity are responsible for a different outcome of each measurement. This error due to fluctuations in temperature and humidity can be minimized by repeating the measurement and averaging the results, and by controlling the temperature and humidity. When multiple crystals are used, their common-mode response may be used to cancel these effects. 

\subsection{Coating}
Coating the crystals is expected to modify the frequency response of the crystal, since it is loaded with a film of polymer. The resonance peak before and after coating a crystal with PEE can be found in \autoref{fig:linear_damping}. The coating has decreased the resonance frequency of the crystal and the amplitude of the response, as was expected due to mass loading. The width of the peak has also increased, corresponding to a lower Q-factor, which indicates an increased damping, possibly due to viscous losses in the polymer. The crystals coated with PAA, PDMS and PEG showed behavior similar to the crystal coated with PEE. 

\begin{figure}[!htb]
	\centering
		\includegraphics[width=0.86\textwidth]{figures/linear_damping.pdf}
	\caption{Frequency response of a crystal driven at $V_\mathrm{ref}=\SI{40}{\milli\volt}$ before and after coating with PEE are simultaneously plotted. The same $x$-scale is used for both graphs, offset by the frequency shift of approximately \SI{775}{\hertz}.}
	\label{fig:linear_damping}
\end{figure}

The shift of the hysteresis regime for the coated crystal is shown in \autoref{fig:hysteresis_regime}. It takes a significantly higher amplitude for the crystal to show bistable behavior. The amplitude of mechanical movement is smaller due to higher damping and mass, and therefore the significance of nonlinear effects, which scale with $x^3$. This causes the critical point to occur at a higher $V_\mathrm{ref}$. 
\begin{figure}[!htb]
	\centering
		\includegraphics{figures/PEE_hysteresis_together.pdf}
	\caption{The hysteresis area of an uncoated crystal (right spike) and the same crystal after coating with PEE (left spike) are simultaneously plotted. The figure is obtained by superimposing the upward and downward frequency sweep traces. Both data sets cover a bandwidth of $\SI{100}{\hertz}$.}
	\label{fig:hysteresis_regime}
\end{figure}




\subsection{Gas sensing}
Now that the crystals are coated, the sensitivity to gasses in the using crystal driven in the linear and nonlinear regime can be compared. \autoref{fig:sensitivity} shows a crystal coated with PEG driven at different voltages in air at reduced pressure and in water vapor. There is a clear jump in the resonance frequency of the crystal after the introduction of water vapor. The width of the resonance peak has also widened, and the nonlinearity has reduced, as can be seen from the resonance peaks at a driving voltage of $V_\mathrm{ref}=\SI{90}{\milli\volt}$. In air, the peak shows a jump in frequency at this driving voltage, while the crystal has not yet reached bistability in water vapor. These results indicate that the water vapor has been adsorbed on the coating of the crystal, increasing the mass and damping.\footnote{Although the pressure has also slightly increased in the case of water vapor, the frequency difference of the crystal in air at atmospheric pressure and \SI{1}{\kilo\pascal} due to decreased damping is only a few Hz, so the effect of pressure is negligible in this experiment} It is interesting to note that the widening of the peak is significantly bigger than just for the coating in \autoref{fig:linear_damping}, while the jump in frequency is much lower. This indicates that the effects of damping are more prominent than the effects of mass loading in the case of gas adsorption in comparison to coating. 

\begin{figure}[htb!]
	\centering
  \begin{subfigure}{13cm}
		\includegraphics[trim=0 1.1cm 0 0, clip,width=0.97\textwidth]{figures/sensitivity_PEG_clean}
  \end{subfigure}
	\\
	\begin{subfigure}{13cm}
		\includegraphics[width=0.97\textwidth]{figures/sensitivity_PEG_water}
  \end{subfigure}
	\caption{Normalized upward sweep frequency responses of a crystal coated with PEG in air at a measured pressure of $\SI{1}{\kilo\pascal}$ (top) and the same crystal in water vapor at room temperature (bottom), which has a vapor pressure of $\SI{2}{\kilo\pascal}$\cite{vapor}. \label{fig:sensitivity}}
\end{figure}

The distance between the peaks of responses with the same driving voltage in air and in water vapor decreases as the driving voltage is increased. The frequency change due to mass loading of adsorbed particles is smaller in the nonlinear regime than in the linear regime. From the flattening of the curve of the crystal in water vapor at $V_\mathrm{ref}=\SI{170}{\milli\volt}$, it seems that the damping or mass is decreased during the sweep. This could be due to heat-up of the crystal at higher amplitude, causing the adsorbed particles to evaporate. 

\begin{figure}[htb!]
	\begin{subfigure}{13.7cm}
		\includegraphics{figures/smelling}
  \end{subfigure}
	\caption{Normalized upward sweep frequency responses in three environments of three crystals with different treatments (left: PEG coating; middle: uncoated and right: PEE coating) connected in parallel. The measured air pressure is $\SI{1}{\kilo\pascal}$; for water and ethanol the vapor pressures at room temperature are $\SI{2}{\kilo\pascal}$ and $\SI{6}{\kilo\pascal}$ respectively\cite{vapor}. The sweep time of each trace is approximately \SI{1}{\minute}. \label{fig:smelling}}
\end{figure}

To demonstrate the selectivity of different coatings, crystals coated with PEE and PEG are placed in parallel with a crystal without coating. The result of frequency sweep in different environments \autoref{fig:smelling} shows that ethanol has a stronger effect on the PEG coated crystal response, whereas water has a stronger effect on the PEE coated crystal response. After exposure to the gasses, the settling time of the resonance frequencies was in the order of seconds, but seemed to be lower for water adsorption\footnote{The settling time is difficult to determine with the current set-up due to the relatively slow frequency sweeps. A better method would be to build an oscillator with the crystals and track the oscillation frequency.}. The difference in adsorption and desorption rates might be used as extra information when determining the presence of gases by driving the crystal with a temperature cycle. 

While the crystals show the jump phenomenon in air, the coated crystals lose their bistability in the environments they most respond to. This should be taken into account when attempting to use the method in \autoref{fig:splitting} to distinguish the peaks of the responses. The amount of coating and the drive signal should be chosen in such a way to keep the crystals in their bistable regimes for the expected concentrations of gasses.

The crystals coated with PAA and PDMS barely showed any change in response, just like the uncoated crystal. There could be different reasons for this behavior. First of all, it could be that the gasses don't adsorb very well on these coatings. Water is a bad solvent for PDMS, and ethanol is a polar molecule like water, so this is reasonable for PDMS. However, PAA should be a very good adsorbent for water, since it can absorb its own weight in water many times.\cite{polyacrylates} The second reason could be that these coatings don't stick to the crystal very well and just evaporated in the vacuum. In the case of PAA, it could also be that the molecules already absorbed water from the environment, and that the binding force is strong enough to keep the molecules from evaporating in a low vacuum. 
\clearpage\section{Materials \& Method}
Before proceeding with the description of the experimental set-up, we will start with the selection of a quartz crystal suitable for the experiment. Afterwards, the electronic system will be explained, simulated and subsequently tested, and finally the gas sensing set-up will be described. 

\subsection{Crystal selection}
Quartz crystals come in a variety of shapes and sizes and corresponding resonance frequencies. For this experiment a large crystal is preferred over a small crystal, since it facilitates polymer coating and general handling of the crystal. The fundamental frequency is preferably small, since it loosens bandwidth requirements of all components in the circuit and reduces crosstalk and phase lag. The most common crystal shapes are thickness shear mode resonators which generally have fundamental frequencies between \SIrange{1}{30}{\mega\hertz}, and tuning fork crystals which typically have a fundamental frequency of \SI{32768}{Hz}.\footnote{\SI{32768}{\hertz} corresponds to $2^{15}$~\SI{}{\hertz}, a frequency which can easily be converted to \SI{1}{Hz} for clocks with digital dividers.} Although the tuning forks have a desirably low frequency, their resistance is typically 2-3 orders of magnitude higher than thickness shear mode resonators, and initial tests have shown that by driving them with high amplitudes, they will break before showing significant nonlinearity. Therefore they are not suitable for this experiment, and the used crystal will be of the thickness shear mode type. An additional feature of these devices is their large flat surface, which eases polymer coating. 

Aside from the previously mentioned reasons to minimize the fundamental frequency, low frequencies generally have a higher resistance, since the fundamental frequency is inversely proportional to the thickness of the crystal. This is beneficial because of the lower current and corresponding lower effect of stray inductance and self heating, which will also be shown in the results. Approximately 15 different cut crystals of different fundamental frequencies and various manufacturers have been tested for their nonlinearity. The crystal which was most promising was HC49U-4.608-20-50-60-30-ATF (\autoref{fig:chosen}), due to the prevalence of highly nonlinear and high resistive modes. This AT-cut crystal has a parallel resonance frequency of \SI{4.608}{\mega\hertz} in a HC-49/U package, which is a resistance welded package with an atmosphere of nitrogen to minimize degrading of the crystal. The crystals used in this experiment have had their protective package removed, which notably decreased heat-up of the crystals. After a week of exposure to air, the crystal electrodes started to show oxidation, which is why inert electrodes are usually used for crystals without a protective package. The crystals used in the experiments were used within a few days after opening and did not yet show any deterioration.

\begin{figure}[!htb]
	\centering
		\includegraphics[width=0.8\textwidth]{figures/4608crystal.pdf}
	\caption{A specimen of HC49U-4.608-20-50-60-30-ATF (datasheet: \cite{datasheet}), the quartz crystal used in this experiment, inside its protective package (left) and after removing the package (right).}
	\label{fig:oveele}
\end{figure}

\subsection{Electrical system}\label{sec:elec}
A basic overview of the electrical system is shown in \autoref{fig:oveele}. The signal of a lock-in amplifier is amplified and buffered before exciting the crystal. The current is probed and this signal is fed back into the lock-in amplifier to measure the response of the crystal. The current flowing through the crystal is indicative of the impedance of the crystal, and thus whether the crystal is on resonance. 

\begin{figure}[!htb]
	\centering
		\includegraphics{figures/oveele.pdf}
	\caption{Basic overview of the electrical system of the experimental set-up. }
	\label{fig:chosen}
\end{figure}

Since the impedance of the crystal changes orders of magnitude during a frequency sweep, and multiple crystals in parallel are an especially capacitive load, a driving circuit is needed which is stable at a wide range of loads. Standard RF amplifiers, such as the ZHL-32A+ used in this set-up, have an output impedance of \SI{50}{\ohm} to match the impedance of the cable and other standard devices in order to minimize reflections and maximize power transfer. For a crystal with a varying impedance, this output impedance would act as a voltage divider and the output voltage would change as the load changes. Therefore the output of the amplifier must be buffered to minimize the output impedance. 

Preliminary tests where the crystal was driven with the output of an Agilent 4395A Vector Network Analyzer amplified with a ZHL-32A+ showed that to be able to reach the desired nonlinearity a voltage of about $V_{p}=\SI{10}{\volt}$ is needed.\footnote{In this experiment we actually will not use a voltage above  $V_{p}=\SI{2.5}{\volt}$, but the same set-up is used for the relaxation oscillator described in \autoref{ap:relaxation}, which uses much higher voltages.} The maximum current at this voltage is about $I_{p}=\SI{200}{\milli\ampere}$ for the lowest resistance mode. These values are the main requirements for the choice of the driving circuit, together with the wish to have a bandwidth which allows us to drive the third harmonic which is at approximately \SI{14}{\mega\hertz}. Since there is no single amplifier with feedback available which can fulfill all these requirements, a high speed operational amplifier was used in combination with a buffer amplifier in the feedback path to decrease the output impedance and increase the output current capability of the amplifier. 

The slew rate of the amplifier is an important parameter, since the signal has both high frequency and high amplitude. The needed slew rate corresponds to the maximum slope of the output signal, which for a sinusoidal signal of the form
\begin{equation}\label{eq:driving}
v(t) = V_{P} \sin{2\pi ft},
\end{equation}
is given by
\begin{equation}\label{eq:slewrate}
\mbox{max}\left(\left|\frac{dv(t)}{dt}\right|\right) = \frac{\pi fV_{P}}{2}.
\end{equation}
Using frequency of $\SI{15}{\mega\hertz}$ and a peak voltage of $V_\mathrm{p}=10$~V, the minimum slew rate of the amplifier should therefore be \SI{235}{\volt/\micro\second}. 

Since the buffer will be inside the feedback loop of the operational amplifier, the added phase change at high frequencies puts the circuit at risk to oscillate. This can be remedied by using a unity gain stable operational amplifier and short-circuiting high frequencies at the output of the operational amplifier to its inverting input, forcing them at a stable unity gain. 

\begin{figure}[!b]
	\centering
		\includegraphics[width=\textwidth]{figures/BUF634_paper.pdf}
	\caption{Detailed schematic of the electrical system of the experimental set-up as used in LTspice. $V_\mathrm{in}$ corresponds to the reference voltage $V_\mathrm{ref}$ of the HF2LI lock-in amplifier amplified by the +\SI{25}{\decibel} ZHL-32A+ amplifier. The \emph{OUT} node corresponds to the signal that is fed back into the lock-in amplifier, and the \emph{CRYSTAL} node is the signal which is directly driving the crystal. During simulation, crystal X1 is substituted by parallel resistance and capacitance to characterize the circuit under load. The supply voltages of the active components all have the manufacturers' recommended bypass capacitances which are not drawn in the circuit to decrease clutter.}
	\label{fig:detsch}
\end{figure}

This stringent set of requirements can be fulfilled with the circuit in \autoref{fig:detsch}, using an LT1363 (U1) as the operational amplifier and a two BUF634s (U2 \& U3) as the buffer in the feedback path. Two BUF634s were put in parallel to increase the output current capability of the buffer, with some extra output resistance to stop the buffers from driving each other due to offset voltages. R4 is used as a current to voltage transducer which is amplified with another LT1363 (U4) used as a differential amplifier, since the voltage over the resistor will have a common mode offset of approximately the driving voltage. The voltage which is fed back to the operational amplifier is connected after this sense resistor, so the voltage of the resistor does not affect the voltage over the crystal. Also note the $RC$-filter in the feedback path, effectively shorting high frequencies to be at a stable unity gain. The driving signal is generated by a HF2LI lock-in amplifier and amplified by a ZHL-32A+, and the output of the differential amplifier is fed back into the lock-in amplifier. 



Before implementing, the circuit was simulated in LTspice IV with the manufacturers' component models, using the simulation model in \autoref{fig:detsch}. A frequency response of the circuit is shown in \autoref{fig:sim_crystal}, with the \emph{CRYSTAL} node as output and with the \emph{OUT} node as output, which is the signal corresponding to the current flowing through the crystal and which is fed back to the lock-in amplifier. The simulation predicts an almost flat response up to about \SI{30}{\mega\hertz}, after which it starts to roll off for the \emph{CRYSTAL} node, although there is a small peak for the other node. This means that the bandwidth leaves some room for error to reach the desired \SI{14}{\mega\hertz}. There will be some noise amplification due to the peak, which could be improved by adding feedback capacitors to the differential amplifier. However, due to the difficulty of exactly matching the capacitors and because the lock-in amplifier acts as a narrow bandpass-filter around the driving frequency, the peak will not significantly alter the circuits' performance. 

\begin{figure}[!htb]
	\centering
		\includegraphics{figures/sim_out.pdf}
	\caption{Frequency response of the LTspice simulation of the circuit in \autoref{fig:detsch} with the \emph{CRYSTAL} and the \emph{OUT} nodes as output.}
	\label{fig:sim_crystal}
\end{figure}

It is important that the amplitude of the voltage which is directly driving the crystal is stable over a wide range of loads, so it does not skew the amplitude-frequency dependence measurements. A simulation of the voltage at the crystal for different resistive loads is shown in \autoref{fig:load_response}, together with the measured values of the circuit. The values are measured using a 10:1 probe which also loads the circuit with \SI{9.5}{\pico\farad} and \SI{10}{\mega\ohm}, but by adding capacitance, the output does not significantly change up until \SI{330}{\pico\farad}, after which it becomes unstable. This is also well over the parasitic capacitance of about \SI{5}{\pico\farad} of a crystal, so this capacitive loading is unlikely to cause any problems. 


\begin{figure}[!htb]
	\centering
		\includegraphics{figures/load_response.pdf}
	\caption{Simulated and measured responses to resistive loads for different circuits. The blue line shows the calculated response that a standard amplifier with \SI{50}{\ohm} output impedance would have. The other traces are the responses of the buffered circuit, where the orange line corresponds to the simulation of \autoref{fig:detsch}, and the discrete points are measured responses of the implemented circuit at different input voltages.}
	\label{fig:load_response}
\end{figure}

The measured results show a slightly different trend than the simulation, but the error is less than \SI{2}{\percent} at $R\geq \SI{18}{\ohm}$. An amplifier with an output impedance of \SI{50}{\ohm} would have this error at \SI{2.45}{\kilo\ohm}, with the error rapidly increasing for lower load impedance. At high driving voltage and very low resistance the error becomes exceedingly high, which happens because the maximum output current of the buffer is reached. Since the crystals used in this experiment will have $R> \SI{20}{\ohm}$ and will not be driven at a voltage higher than $V_\mathrm{p} = \SI{2.5}{\volt}$, the driving circuit is a very stable voltage source in this regime. 

\subsection{Gas set-up}
To demonstrate the selectivity of the crystals, they are exposed to different gasses using the set-up in \autoref{fig:ovegas}, which is based on a set-up which has previously been used for gas sensing using nonlinear resonant cantilevers.\cite{0957-4484-25-42-425501} The set-up consists of vessels with liquids which are separated from a chamber containing the crystals using manual valves S1 and S2. The crystal chamber is purged using a vacuum pump, and a liquid can be selected to be vaporized by opening the corresponding valve. There is an extra valve S4 connected to the chamber to be able to ventilate the chamber before pumping to decrease the water and ethanol intake of the pump, which decreases the pump performance. 

\begin{figure}
	\centering
		\includegraphics[width=\textwidth]{figures/gas_mixing_BEP.pdf}
	\caption{Schematic overview of the gas to  system of the experimental set-up. }
	\label{fig:ovegas}
\end{figure}

The vacuum pump used is a VE115N oil pump which can attain a maximum vacuum of \SI{2}{\pascal} according to the manufacturer. The pressure is measured using a KJL300806 Pirani gauge, which shows that the pressure attained is about \SI{40}{\pascal}, which is an order of magnitude higher than the pump's maximum performance. This discrepancy can be attributed to the use of push-fit connectors in the set-up to simplify prototyping. 

The liquids in the vessels which are used in this experiment are ethanol and water, which have a vapor pressure of $\SI{6}{\kilo\pascal}$ and $\SI{2}{\kilo\pascal}$ at room temperature respectively.\cite{vapor} The Pirani gauge shows a pressure of between $\SI{1}{\kilo\pascal}$ and $\SI{2}{\kilo\pascal}$ for both vapors. The measured values were expected to be higher than the actual values, since the gauge has been calibrated for nitrogen, and the higher thermal conductivity and heat capacity of water and ethanol would absorb more heat like a higher pressure of nitrogen would do. Since there is no suitable conversion table for these gasses for this particular gauge we will assume that the vapor pressure is the cited vapor pressure at room temperature. Note that the vapor pressures of ethanol and water are orders of magnitude higher than the vacuum pressure of \SI{40}{\pascal}, so the vapors can be safely assumed to be pure. 

After characterizing the crystals and analyzing their nonlinearity, the crystals will be coated with the following polymers: 
\begin{itemize}
\item Poly(acrylic acid) (PAA);
\item hydroxy-terminated Poly(dimethylsiloxane) (PDMS);
\item branched Polyethylenimine (PEE); 
\item	Polyethylene glycol solution (PEG).
\end{itemize}
These polymers were chosen because they are water-soluble\footnote{Although PDMS is water-soluble, water is not a \emph{good} solvent for PDMS, so crystals coated with PDMS will likely show a decreased response to water in comparison to crystals coated with the other polymers.} and therefore did not need any exotic solvents, and because they are relatively harmless substances. The coating is performed by dipping the crystals, wiping off any excess solution and subsequently placing the crystal in the vacuum chamber to speed up evaporation of the solvent. The gas set-up will be used to investigate the response of the coated crystals when exposed to ethanol and water. 
%TODO: calculate expectation difference in frequency

\clearpage\section{Theory}
Before describing nonlinearity in quartz crystals, the phenomenon of linear resonance will be explained using the analogue between a mechanical and an electrical system. Afterwards, piezoelectricity is discussed, and the frequency behavior of quartz crystals using a simple electrical equivalent model will be studied. The model will then be expanded with a nonlinear term, which yields the Duffing equation and gives rise to the nonlinear jump phenomenon. Throughout the section attempts will be made to predict the behavior of the crystal in various experimental settings.

\subsection{Resonance}\label{sec:resonance}
A piano string vibrating after it has been hammered, a swing in the play yard which has been pushed by a kid and an electronic system consisting of inductors and capacitors all have behave in a similar manner. These systems tend to oscillate at a certain frequency\footnote{The system can actually oscillate at many frequencies simultaneously, as is especially evident in the case of the piano string, giving it its rich sound, but this is not important for the current discussion.}. If you try to push the swing higher, its amplitude will increase, but (apart from nonlinear effects which will be covered later) the frequency of the swinging will remain the same. This tendency of a system to oscillate at certain frequencies when it is driven is called \emph{resonance}. 

\begin{figure}[!h]
\captionsetup[subfigure]{position=b}
	\centering
	\subcaptionbox{Spring-mass system}{\includegraphics{figures/spring-mass.pdf}}\hfill%[width=0.35\textwidth]
	\subcaptionbox{Series $RLC$-circuit}{\includegraphics{figures/RLC.pdf}\vspace{1cm}}%[width=0.65\textwidth]
	\caption{Two classical examples of oscillatory systems which are governed by the same type of second order differential equation.}
	\label{fig:oscillatory_systems}
\end{figure}


Two classical systems commonly used to explain resonance are the spring-mass system and an $RLC$-circuit (\autoref{fig:oscillatory_systems}). While the fundamental laws governing these mechanical and electrical systems are completely different, they behave very much alike. The equation describing the motion of the mass $m$ of the spring mass system is
\begin{equation}\label{eq:spring-mass}
\frac{d^2u(t)}{dt^2} + \left(\frac{b}{m}\right) \frac{du(t)}{dt} + \left(\frac{k}{m}\right) u = \frac{F_\mathrm{ext}(t)}{m},
\end{equation}
with $u$ the displacement from the equilibrium point, $k$ the spring constant and $b$ the damping force proportional to the speed $du/dt$, driven by an external force $F_\mathrm{ext}$.\cite{dynamics} For the $RLC$-circuit, the charge $q$ on the capacitor is given by 
\begin{equation}\label{eq:RLC}
\frac{d^2q(t)}{dt^2} + \left(\frac{R}{L}\right)\frac{dq(t)}{dt} + \frac{1}{\sqrt{LC}} q = V(t),
\end{equation}
when driven by a voltage source $V$.\cite{dynamics} Evidently both simple oscillating systems can be described by a similar differential equation, where $q$ is the electrical analogue of the displacement $u$, and $I=dq/dt$ corresponds to $v=du/dt$. Furthermore, by comparing the prefactors of coefficients $u$ and $q$, and those of their derivatives, there are a few more analogues, which are listed in \autoref{tab:analogues}. 


\begin{table}[!htb]
	\centering
	\caption{Analogues of a mechanical system, such as a spring-mass system, and an electrical system, such as an $RLC$-circuit, which can both be described by the same type of second order differential equation.\label{tab:analogues}}
	\begin{tabular}{ccc}\toprule
	Mechanical & $\Leftrightarrow$ & Electrical\\ \midrule
	$u$	&&	$q$\\
	$v$ && 	$I$\\
	$b$ && 	$R$\\
	$m$ &&	$L$\\
	$k$ &&	$1/C$\\ \bottomrule
	\end{tabular}
\end{table}

To generalize the equations of our oscillating systems, we introduce a damping term $\delta$, which corresponds to $R/L$ or $b/m$, and we will call the factors $\sqrt{k/m}$ or $1/\sqrt{LC}$ the natural frequency $\omega_n$, of which the significance will be clear shortly. This yields 
\begin{equation}\label{eq:resonance}
\ddot{x} + \delta \dot{x} + \omega_n^2x = g(t),
\end{equation}
where $x$ can be any oscillating quantity and $g(t)$ is a driving function. To see how the system behaves at different frequencies, we take a look at the Fourier transform of \autoref{eq:resonance}:
\begin{equation}\label{eq:frequency_response}
H(j\omega)= \frac{1}{(j\omega)^2 + \delta(j\omega) + \omega^2_n}.
\end{equation}
Plotting this function for different values of $\delta$ (\autoref{fig:damping}) shows that the system indeed has a preference to oscillate at a certain frequency as long as the damping is not too high. 

\begin{figure}
	\centering
		\includegraphics{figures/damping.pdf}
	\caption{Transfer function $H$ of resonating systems described by a second order differential equation of the form of \autoref{eq:resonance}. The shape of the graph is independent of $\omega_n$.}
	\label{fig:damping}
\end{figure}

For low damping, the \emph{resonance frequency}, which is the frequency at which the peak in the transfer function occurs, is very close to the natural frequency $\omega_n$. These are also sometimes called the \emph{damped} and \emph{undamped resonance frequency} respectively. As the damping increases, the resonance frequency becomes lower and the peak value becomes smaller and wider. The width of the resonance peak is easy to measure experimentally by doing a frequency sweep and is directly related to a useful characterizing parameter of resonators called the \emph{quality-factor} (or \emph{Q-factor}) as given by 
\begin{equation}\label{eq:Q}
Q = \frac{\omega_0}{\Delta\omega},
\end{equation}
where $\Delta\omega$ is the bandwidth in which the power of the oscillation is more than half the power at the resonance frequency.\cite{electroniccircuits} Systems with a narrow peak therefore have a high quality factor, and from \autoref{fig:damping} we see that they correspond to devices with low damping. The Q-factor is an important parameter when a single detector is used to locate multiple peaks, which is the case for a sensor array. It determines the amount of space needed between resonance peaks to be able to discern them, and therefore drives the required frequency sweep measuring bandwidth. Combined with the fact that energy consumption is lower due to low damping for high Q-devices, a high Q-factor is generally preferable.

Although this section focused on simple harmonic oscillators, there are many other types of oscillators, such as relaxation oscillators, which are described in more detail in \autoref{ap:relaxation}. By increasing the amplitude of oscillations, the differential equations used in this section are no longer valid and nonlinear effects start to play a role. This will be used to decrease the required measuring bandwidth without needing high quality factor devices, which will be further explained in \autoref{sec:nonlinear}. 

\subsection{Piezoelectricity}
First discovered in quartz (SiO$_4$) by the Curie brothers in 1880\cite{curie}, piezoelectricity has proven to be a phenomenon with a great variety of applications. After the first quartz crystal oscillator was built by Walter Guyton Cady in 1921, it has been used as a highly stable frequency reference, serving as the basis for the unit of time by the National Institute of Standard and Technology (NIST)\cite{NIST} before the advent of atomic clocks in the 1950s. Since then it is used as a cheap and accurate method for timekeeping in watches and digital electronics. A picture of a quartz crystal that is used in electronics (and incidentally also in this experiment) is shown in \autoref{fig:HC49}. More recently, piezoelectricity is used to make tactile sensors for robotic applications\cite{robot} and it is rising in popularity for use in energy harvesting, such as having piezoelectric materials embedded in shoes\cite{shoe}. 

\begin{figure}[b!]
	\centering
			\includegraphics[width=0.7\linewidth]{figures/InsideQuartzCrystal.pdf}
	\caption{Quartz crystal widely used in consumer electronics sealed in a vacuum or an inert gas to decrease damping and aging (left) and with an opened casing, showing the structure and the sources of parasitic capacitances (right). (Image source: \cite{open_crystal})}
	\label{fig:HC49}
\end{figure}


\begin{figure}
	\centering
		\includegraphics[width=\textwidth]{figures/piezo_crystal.pdf}
	\caption{Atomic model of a piezoelectric crystal. (a) One unit cell at rest. (b) The unit cell becomes a dipole as it is strained by an external force. (c) The effect on a macroscopic scale; as the crystal is strained, the dipoles form a net charge distribution. (Image source: \cite{robot})}
	\label{fig:piezo_crystal}
\end{figure}

Having its etymological roots from the Greek \emph{piezein}, which means ``to press tight''\cite{etymology}, piezoelectricity couples the strain of an object to its electrical charge distribution. When a piezoelectric material is strained, its crystal structure changes in such a way that the unit cells form dipoles. As shown in \autoref{fig:piezo_crystal}, these dipoles will generate a charge on the surface of the crystal. In the case of quartz, the opposite is also true: when an electric field is applied from one surface to the other, the material will be strained. Since quartz is an anisotropic material, the direction in which the crystal is cut changes its properties, such as its temperature dependence and the way the crystal is strained when an electric field is applied. Different cuts have been developed to suit different applications. In this paper we cover an AT-cut crystal, which is the most popular crystal cut because of its ease of manufacturing and decent temperature stability in a wide temperature range.\cite{cerda2014understanding} \autoref{fig:AT} shows the temperature-frequency relationship of AT-cut crystals at slightly different angles. The crystal has one or two temperatures at which it has a flat slope and therefore is the most stable, dependent on the precise angle of the cut. 

\begin{figure}
	\centering
		\includegraphics[width=0.6\textwidth]{figures/AT_temperature.png}
	\caption{Frequency-temperature-angle characteristics of AT-type quartz resonators. (Image source: \cite{4051918})}
	\label{fig:AT}
\end{figure}

%Piezoelectric devices exist with a Q-factor of up to XXX, but they are very expensive. Ceramic resonators are cheap, but they lack stability and have low Q-factors XXX. A good middle ground is a quartz crystal oscillator, since they have Q-factors up to $10^6$, and are readily available because of their use in almost any electronic device on the market. 

\subsection{Quartz crystal model}\label{sec:model}

The mechanical elasticity, inertia and damping of the crystal can be equivalently modeled with an inverted electrical capacity $1/C$, inductance $L$ and resistance $R$, respectively, as explained in \autoref{sec:resonance}. Using this analogy it is possible to describe the behavior of the crystal with electronic symbols and network theory, which is highly beneficial, since the actuation and measurement circuitry is also electrical. The simplest quartz crystal equivalent electronic circuit is known as the \emph{Butterworth-Van-Dyke} (BVD) circuit and is shown in \autoref{fig:BVD}. In this circuit, the $C_0$ branch is a parasitic capacitance, consisting of the capacitance of the plates, leads and package shown in \autoref{fig:HC49}. The $RLC$-branch corresponds to a resonance mode of the crystal with equivalent values $R_1$, $L_1$, and $C_1$. 

\begin{figure}[!htb]
	\centering
		\includegraphics{figures/BVD/BVD.pdf}
	\caption{A simple equivalent electrical circuit for the piezoelectric quartz crystal, called the Butterworth-Van-Dyke (BVD) circuit. The $RLC$-branch corresponds to a resonance mode of the crystal, and the $C_0$ is a parasitic capacitance.}
	\label{fig:BVD}
\end{figure}

In general, the value of $R_1$ can range somewhere from $\SI{10}{\ohm}$ to $\SI{50}{\kilo\ohm}$, while the values of the reactive components are of the order of $\SI{0.1}{\femto\farad}$ for the capacitance and $\SI{1}{\henry}$ for the inductance\cite{cerda2014understanding}. Since an inductor of $\SI{1}{\henry}$ already has a parasitic capacitance of orders higher than that of the equivalent capacitance, an $RLC$-circuit built with discrete components with these values would be impossible. These values for the capacitance and inductance translate to a sharp rise in the reactive part of the impedance around resonance, creating a high Q-factor device, which explains the popularity of quartz crystals as time-keeping devices.  

The $RLC$-branch of the BVD circuit corresponds to a single resonance mode of the crystal. This resonance is called the \emph{fundamental frequency} of the crystal. The fundamental frequency of AT-cut crystal corresponds to a thickness shear mode, of which the frequency is directly related to its thickness with approximately \SI{1.7}{\mega\hertz\per\milli\meter}, which originates from the wave propagation speed in quartz.\cite{sauerbrei} This places an upper limit on the fundamental frequency of thickness shear mode resonators, since the thin crystals with frequencies above \SI{30}{\mega\hertz} are prone to breaking during manufacturing. 

Apart from resonance at the fundamental frequency, the crystal can resonate at a great amount of different resonance modes, as can be seen in the result of a frequency sweep of an AT-crystal in \autoref{fig:orientation_scan}. The large resonance peaks occurring close to odd-multiples\footnote{Since opposite sides of the crystal must be out of phase when there is a voltage over the crystal, even-multiples of the fundamental shear mode can not exist in a quartz crystal.} are called \emph{harmonic resonance modes} and correspond to higher order thickness shear modes. Close to the harmonic resonance frequencies, there are many small resonance peaks which most usually correspond to flexural and face-shear modes\cite{spurious}. These modes are normally referred to as \emph{spurious} or \emph{unwanted modes}, since a crystal in an oscillator circuit could sometimes start oscillating in one of these modes, causing all sorts of problems in devices which use crystals. Crystal designers try to maximize the equivalent resistance of these spurious modes to limit the chance of a crystal oscillator accidentally oscillating at a spurious mode. In the results the high resistance of these ``unwanted'' modes will prove to be of great use, since their high resistance limits the current and consequently the heat-up and losses in parasitic inductance. 

To account for the modes besides the fundamental mode, the BVD circuit can be expanded with extra branches with different values for $R$, $L$ and $C$ as shown in \autoref{fig:BVD_extended}. When making a sensor array, multiple crystals can either be put in parallel or in series. Since the impedance of a crystal is very high outside of its resonance frequency, the off-resonance crystals in a series path would block any current. Therefore the crystals will be put in parallel. When multiple crystals are connected in parallel, it follows that this system can be described by the same extended BVD-model, where all the resonance modes of the individual crystals are modeled with an extra $RLC$-branch. The parasitic capacitance of the crystals can be summed to get the new parameter $C_0$. When driving the array with a voltage source, all $RLC$-branches will be driven independent of the current going through the other crystals, assuming an ideal voltage source. In reality the source will have non-zero output impedance, and there will be some mode coupling\cite{mode_coupling}, which could be modeled for example by adding coupling transformers between branches. However, we will assume that there is no coupling between any of the modes, and the extended BVD-model of \autoref{fig:BVD_extended} will be used. 

\begin{figure}[!htb]
	\centering
		\includegraphics{figures/BVD/BVD_extended.pdf}
	\caption{An extended version of the BVD circuit of \autoref{fig:BVD}. The extra $RLC$-branches correspond to spurious and overtone modes of a single crystal. This model could also be used to describe a system of parallel crystals, where the $R_iL_iC_i$-branches correspond to all the resonance modes of the crystals, with a total parasitic capacitance of $C_0$.}
	\label{fig:BVD_extended}
\end{figure}

\subsection{Crystal resonance}\label{sec:crystal_resonance}
From the simple BVD model of \autoref{fig:BVD}, we can see that if $|Z_{R_1}+Z_{C_1}+Z_{L_1}|<<|Z_{C_0}|$ and $Z_{R_1}<<|Z_{C_1}+Z_{L_1}|$, the circuit reduces to a simple $LC$-circuit with a resonance frequency of 
\begin{equation}\label{eq:natural_resonance}
\omega_0 = \frac{1}{\sqrt{L_1C_1}}.
\end{equation}
In general this is a good approximation\cite{cerda2014understanding}, but in this research we will investigate resonance modes of higher resistance and put crystals in parallel. This increases and decreases $Z_{R_1}$ and $Z_{C_0}$ respectively, so we will have a more critical look at the effects on the resonance. 

Since the model consists of two parallel branches, we can add the inverse impedance of the resonant and the parasitic branch to find the inverse of the combined impedance:
\begin{equation}\label{eq:inverseZ}
\frac{1}{Z_\mathrm{crystal}} = \frac{1}{1/(i\omega C_0)} + \frac{1}{R_1 + i\omega L + 1/(i\omega C_1)}.
\end{equation}
Solving for $Z_\mathrm{crystal}$ and separating the real and imaginary parts of the numerator and the denominator of the relation we find:
\begin{equation}\label{eq:Z_crystal}
Z_\mathrm{crystal} = \frac{L_1 - \frac{1}{\omega^2 C_1} + i \left[\frac{-R_1}{\omega}\right]}{R_1 C_0 + i \left[\omega L_1 C_0 - \frac{C_0}{\omega C_1} - \frac{1}{\omega}\right]}.
\end{equation}

We could take the magnitude of \autoref{eq:Z_crystal} and compute the frequency at which $|Z_\mathrm{crystal}|$ is minimal, and thus find its resonance frequency, for a given set of parameters. For example, using typical values of $R_1=\SI{100}{\ohm}$, $L_1 = \SI{1}{\henry}$, $C_1 = \SI{0.1}{\femto\farad}$ and $C_0 = \SI{5}{\pico\farad}$\cite{cerda2014understanding}, the impedance is plotted in \autoref{fig:Z_crystal_example}. We find a minimum impedance at approximately $\omega=\SI{10e8}{\per\second}$ or $f=\SI{16}{\mega\hertz}$, which is reasonable since the fundamental frequency of common quartz crystal cuts range somewhere between $\SI{30}{\kilo\hertz}$ and $\SI{30}{\mega\hertz}$.\footnote{The impedance also goes to 0 asymptotically as frequency rises, but this is ignored since there are always stray inductances which will compensate at high frequencies, and the crystals are driven near their resonance frequencies in this experiment.} Note that the same value would also result from using \autoref{eq:natural_resonance}. 

\begin{figure}[b!]
	\centering
		\includegraphics{figures/crystal_example.pdf}
	\caption{Impedance of a quartz crystal using the BVD-model with parameters $R_1=\SI{100}{\ohm}$, $C_1 = \SI{0.1}{\femto\farad}$, $L_1 = \SI{1}{\henry}$, and $C_0 = \SI{5}{\pico\farad}$.\label{fig:Z_crystal_example}}
\end{figure}

\autoref{fig:Z_crystal_example} shows that for these parameter values, the resonance point coincides approximately with the point at which the impedance transitions from being capacitive to being inductive. This is not surprising, since around the resonance frequency the impedance of the $RLC$-branch is much lower ($\sim\SI{100}{\ohm}$) than the impedance of the parasitic branch ($\sim\SI{2000}{\ohm}$). Therefore the impedance is dominated by the $RLC$-branch here, which makes a transition from capacitive to inductive at the resonance frequency, as the impedance of the inductor rises with frequency, while the impedance of the capacitor decreases. As the impedance of the $RLC$-branch rises when the frequency is increased from the resonance frequency, the impedance of the parasitic branch starts to dominate, causing the total impedance to become capacitive again. This corresponds anti-resonance, since both branches are almost completely out of phase, causing the current through each of them to cancel out. 

To approximate the points of resonance and anti-resonance we can take the reactive part of \autoref{eq:Z_crystal} and set it to 0, to find the points where the capacitive-inductive transitions appear. The following identity is used
\begin{equation}\label{eq:imaginary}
\Im\left\{\frac{a+bi}{c+di}\right\}=\frac{bc - ad}{c^2+d^2},
\end{equation}
to find 
\begin{equation}\label{eq:imzero}
R_1^2 + \left(\frac{\omega^2L_1C_1 -1}{\omega C_1}\right)\left(\frac{\omega^2L_1C_0C_1 - C_0 - C_1}{\omega C_0C_1}\right) = 0.
\end{equation}
In the case where $R_1$ is very small, the first term is negligible and either of the two numerators in the factors must be equal to 0. From this it follows that 
\begin{equation}\label{eq:first_zero}
\omega = \frac{1}{\sqrt{L_1C_1}},
\end{equation}
which again corresponds to the frequency where resonance occurs, but we also find
\begin{equation}\label{eq:second_zero}
\omega = \sqrt{\frac{C_0+C_1}{L_1C_0C_1}},
\end{equation}
which corresponds to the frequency where anti-resonance occurs. It turns out that $C_0$ influences the point of anti-resonance. To check the result, \autoref{eq:Z_crystal} is plotted for different values of $R_1$ and $C_0$ in \autoref{fig:R1C0}. Indeed, $C_0$ mainly changes the point of anti-resonance and hence the place where the phase transitions back to being capacitive. Increasing $R_1$ has the same effect as increasing the damping in \autoref{fig:damping}. It is clear that both increasing $R_1$, which is beneficial because the currents and consequent temperature effects will be lower, and increasing $C_0$, which is inevitable when connecting crystals in parallel, will decrease the Q-factor of the crystal. This increases the bandwidth needed to discern the resonance frequency of different crystals in the linear regime. However, in the next section we will show how we can use nonlinearity to reduce the needed bandwidth. 

\begin{figure}[!htb]
	\centering
	\begin{subfigure}{13.7cm}
		\includegraphics{figures/R1}
	\end{subfigure}
	\begin{subfigure}{13.7cm}
		\includegraphics{figures/C0}
	\end{subfigure}
	\caption{Impedance of a quartz crystal using the BVD-model with parameters $C_1 = \SI{0.1}{\femto\farad}$, $L_1 = \SI{1}{\henry}$, for various values of $R_1$ (top) and $C_0$ (bottom). When varying $R_1$, $C_0=\SI{5}{\pico\farad}$ and when varying $C_0$, $R_1 = \SI{100}{\ohm}$.}
	\label{fig:R1C0}
\end{figure}


Most crystals used in electronics, such as the ones used in the experiment, actually operate at a frequency somewhere between the resonance and the anti-resonance point, and are called \emph{parallel-resonance} crystals. Physically these crystals don't differ from crystals which operate at the \emph{series-resonance} frequency, but the way the crystal is calibrated does. Manufacturers of a parallel-resonance crystal specify extra series capacitance which should be added to the circuit, which moves the transition point from capacitive to inductive impedance further to the right, increasing the resonance frequency. In this experiment we will also use crystals which are specified for parallel-resonance, but since we will use  a circuit which operates at the series frequency, the resonant frequency will be a bit lower than specified in the data sheet. 

\subsection{Nonlinear mechanics}\label{sec:nonlinear}
Because the actuation and measurement system is electrical, it was beneficial to look at the crystal as an equivalent electrical system. This way the effects of parasitic capacitance and damping in the crystal became clear. In the previous section the resonant branch was modeled with an $RLC$-circuit, which can be described by a linear constant-coefficient second order differential equation. Now we will introduce nonlinearity to the system: as the displacement of the crystal from the equilibrium position becomes large enough, the restoring force can no longer be safely approximated by a single linear term. The restoring force $F$ can be expanded as a polynomial of the form
\begin{equation}\label{eq:restoring_polynomial}
F = \sum\limits_{i=1}^n -k_ix^i = - k_1x - k_2x^2 - \dots - k_{n}x^{n},
\end{equation}
where $k_n$ are constants, and $x$ is the displacement due to the deformation. The restoring force should have the same magnitude, but opposing direction when the deformation is in either the positive or in the negative direction. Therefore, the restoring force must be an uneven function, and $k_i=0$ for even $i$.

For small displacements, the first term of the restoring force is dominant, and the restoring force reduces to Hooke's law. As displacements become larger, the first term that becomes significant after $k_1$ is $k_3$. In this experiment the quartz crystals will be modeled by a system with a restoring force of the form of \autoref{eq:restoring_polynomial} with only $k_1$ and $k_3$ non-zero:
\begin{equation}\label{eq:nonlinear_restoring}
F = -k_1x -k_3x^3.
\end{equation}

The differential equation for a system with \autoref{eq:nonlinear_restoring} as the restoring force is called the Duffing equation:
\begin{equation}\label{eq:duffing}
\ddot{x} + \delta \dot{x} + \omega_n^2 x + \alpha x^3 = g(t),
\end{equation}
where $\delta$ again determines the amount of damping, $k_1$ has been absorbed in $\omega_n$ as was done in \autoref{sec:resonance} and $k_3/m$ is defined as $\alpha$, the amount of nonlinearity in the restoring force. 

\begin{figure}[htb!]
	\centering
	\begin{subfigure}{13.7cm}
		\includegraphics[trim=0 1.1cm 0 0, clip]{figures/alpha_r}
	\end{subfigure}
	\begin{subfigure}{13.7cm}
		\includegraphics[trim=0 0.5cm 0 0, clip]{figures/alpha_phi}
	\end{subfigure}
	\begin{subfigure}{13.7cm}
		\includegraphics[trim=0 0 0 9.5cm, clip]{figures/wn}
	\end{subfigure}
	\caption{Frequency response of the Duffing equation with $\delta=0.2$ and $\gamma=2.5$ for different values of nonlinearity $\alpha$.}
	\label{fig:alpha}
\end{figure}

There are many ways to solve the Duffing equation in different limits and under different assumptions. The limit which is most applicable to our case is a weakly nonlinear system with weak periodic driving force $g(t) = \gamma\cos{\omega t}$, where $\delta$, $\alpha$ and $\gamma$ are assumed small. 
The solutions of the Duffing equation are then nearly harmonic and can be approximated in polar co\"ordinates by\cite{duffing}
\begin{equation}\label{eq:duffing_solution}
	\begin{split}
		\dot{r} &= \frac{\omega_n}{\omega}\left(-\delta\frac{\omega}{\omega_n}-\gamma\sin{\phi}\right);\\
		\dot{\phi} &= \frac{\omega_n}{r\omega}\left(-(\omega^2 - \omega_n^2)r + \frac{3}{4}\alpha r^3 - \gamma\cos{\frac{\phi}{r}}\right).
	\end{split}
\end{equation}

The frequency response can be analyzed by finding the equilibrium points of Equations \ref{eq:duffing_solution} by setting $\dot{r}=\dot{\phi}=0$. The results for different values of $\alpha$ can be found in \autoref{fig:alpha}. When $\alpha=0$, the Duffing equation reduces to the simple linear constant coefficient second order differential equation of the form of \autoref{eq:resonance}, and the solution looks like the solutions found in \autoref{fig:damping}. When $\alpha<0$, the spring constant becomes smaller as the amplitude increases, which corresponds to a weakening spring and hence a higher peak response and a lower resonance frequency. Conversely, when $\alpha>0$, the system corresponds to a stiffening spring with a smaller peak response and a higher resonance frequency.

Since the nonlinear force scales with $x^3$, it is expected that the system is almost linear at low driving amplitudes $\gamma$. Indeed, \autoref{fig:gamma} shows that at low $\gamma$, the solution again looks like the result from the linear system of \autoref{fig:damping}. When the driving amplitude is increased, the nonlinearity becomes more pronounced, and at high enough values for $\gamma$, multiple solutions are possible for a given frequency. It turns out that only the outer two of these solutions are stable, while the solution in the middle is unstable.\cite{duffing} Hence, when driving the crystal with an increasing frequency sweep, the crystal will follow the upper branch and will jump down to the lower branch when it reaches the vertical tangent. Similarly, if one would drive the crystal with a decreasing frequency sweep, the crystal response will follow the lower branch and jump up when it reaches the vertical tangent. This behavior causes a jump in the response when subjected to a frequency sweep.

\begin{figure}[!htb]
	\centering
	\begin{subfigure}{13.7cm}
		\includegraphics[trim=0 1.1cm 0 0, clip,width = 0.97\textwidth]{figures/gamma_r}
	\end{subfigure}
	\begin{subfigure}{13.7cm}
		\includegraphics[trim=0 0.5cm 0 0, clip,width = 0.97\textwidth]{figures/gamma_r_sweep}
	\end{subfigure}
	\begin{subfigure}{13.7cm}
		\includegraphics[trim=0 0 0 9.5cm, clip]{figures/wn}
	\end{subfigure}
	\caption{Solutions of the Duffing equation with $\delta=0.2$ and $\alpha=0.003$ for different values of driving force $\gamma$ (top). During an increasing frequency sweep experiment, there would be a jump in the amplitude (bottom).}
	\label{fig:gamma}
\end{figure}

